package gurobi;

import java.util.Arrays;

public class EuQmax implements Euristica {
	/*
	 * (non-Javadoc)
	 * 
	 * @see gurobi.Euristica#ordina(gurobi.ModelVar, gurobi.Lettore)
	 */

	private double prob=0.0909091;
	private double intervallo=0.0909091;
	
	@Override
	public void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException {
		// TODO Auto-generated method stub

		Struttura[][] medici = new Struttura[inst.getN_physicians()][inst.getN_days()];
		int[][] Qmax = inst.getQMAX();
		int[][] matriceD = inst.getMatrix_d();
		GRBVar[][][] x = model.getX();

		for (int p = 0; p < inst.getN_physicians(); p++) {

			for (int d = 0; d < inst.getN_days(); d++) {

				Struttura uno = new Struttura(d);
				medici[p][d] = uno;
				int costo = inst.getQMAX()[p][d] - (int) (model.getZEVal(p, d) - model.getZBVal(p, d));
				if (costo > 0) {
					uno.setCosto(costo);
				} else {
					uno.setCosto(0);
				}

			}

		}

		for (int p = 0; p < inst.getN_physicians(); p++) {
			Arrays.sort(medici[p], new Ordina());
		}

		// Fisso le variabili
		for (int p = 0; p < inst.getN_physicians(); p++) {
			//System.out.println(medici[p][0].getCosto() + "-----------------------");
			for (int d = 0; d < medici[p].length * (percent); d++) {

				int dd = medici[p][d].getIndice();
				model.setW(p, dd);

				for (int j = 0; j < inst.getN_jobs(); j++) {
					model.setX(p, j, dd);
				}

			}
		}

		GRBLinExpr expr = new GRBLinExpr();
		int n = 0;

		// Definisci i vincoli riguardanti i lavori
		for (int p = 0; p < inst.getN_physicians(); p++) {
			System.out.println("-----------------------");
			System.out.println("Vincolo: " + medici[p][inst.getN_days() - 1].getCosto());
			
			// RIMUOVO UN JOB SOLO AL MEDICO PEGGIORE
			int dd = medici[p][inst.getN_days() - 1].getIndice();
			for (int j = 0; j < inst.getN_jobs(); j++) {

				if (model.getXVal(p, j, dd) == 1) {
					n = n + 1;
					expr.addTerm(1.0, x[p][j][dd]);

				}
			}
		}
		model.addCons(expr, n - 1);
	}

	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	public double getIntervallo() {
		// TODO Auto-generated method stub
		return intervallo;
	}

	public void setIntervallo(double intervallo) {
		this.intervallo = intervallo;
	}
}
