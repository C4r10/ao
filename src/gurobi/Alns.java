/**
 *
 */
package gurobi;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Carlo 20 ott 2016
 *
 */
public class Alns {

	/**
	 * @param model
	 * @param q
	 * @param soluzione
	 * @param istanza
	 * @throws GRBException
	 * @throws IOException
	 */
	public void run(ModelVar model, String numeroIstanza, String soluzione, Lettore istanza)
			throws GRBException, IOException {

		boolean stoppingRule = true;

		// Leggo la sol corrente s
		System.out.println("---------------------------------------------------------------------------------------");
		System.out.println("LEGGO SOL CORRENTE");
		System.out.println("---------------------------------------------------------------------------------------");

		leggiSoluzione(model, soluzione);

		System.out.println("---------------------------------------------------------------------------------------");
		System.out.println("INIZIALIZZO LE EURISTICHE");
		System.out.println("---------------------------------------------------------------------------------------");

		// numero iterazioni
		int h = 0;

		double segno = 1; // Negativa o positiva (+1, -1) se l'euristica
		// migliora o peggiora la soluzione

		double q;
		double qMin = 0.4;
		double qMax = 0.8;

		// inizializzo euristiche
		StartJob e0 = new StartJob();
		StartJobDay e9 = new StartJobDay();
		SpecJob e1 = new SpecJob();
		DurataJob e2 = new DurataJob();
		DurataJob2 e10 = new DurataJob2();
		EuQmin e3 = new EuQmin();
		EuQmax e4 = new EuQmax();
		EuFmin e5 = new EuFmin();
		EuFmax e6 = new EuFmax();
		EuUmin e7 = new EuUmin();
		EuUmax e8 = new EuUmax();

		String nomeEuristica = null;
		Double valueObj;
		Double bestObj = model.getObjective();
		double temp = 200;

		GestisciEuristiche probabilita = new GestisciEuristiche(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10);

		Date d = new Date();
		FileWriter logFile = new FileWriter(
				"log/istanza_" + numeroIstanza + "_" + new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss").format(d) + ".csv");
		// intestazione
		logFile.write(
				"ITERAZIONE; VALORE Q; EURISTICA; FUNZIONE OBIETTIVO;PEGGIORATIVA; e0; e1; e2; e3; e4; e5; e6; e7; e8; e9; e10; \r\n");
		logFile.write("0; ; ; " + model.getObjective() + "; ; ; ; ; ; ; ; ; ; ; ;\r\n");

		while (stoppingRule) {

			model.getModel().getEnv().set(GRB.IntParam.SolutionLimit, 30000);
			model.getModel().getEnv().set(GRB.DoubleParam.TimeLimit, 30);

			h++;
			model.restore();
			probabilita.inizializza();

			temp = temp * 0.85;

			// decide number q? lo aumentiamo lo dimuniamo?
			// q = 0.7;// SpecJob ha ancora qualche problemino...
			q = qMin + (Math.random() * (qMax - qMin));// [qMin,qMax)
			System.out.println("VALORE DI Q; " + q);

			// numero random, lo utilizzo per scegliere l'euristica
			double rand = Math.random();
			// System.out.println(rand);
			int peggiorativa = 0;

			System.out.println();
			System.out.println("*******************************************************************");
			System.out.println("                        ITERAZIONE : " + h);
			System.out.println("*******************************************************************");
			System.out.println();

			// Il valore Prob delle euristiche � dato dalla somma dei valori
			// Prob precedenti (nell'ordine di inizializzazione) pi� il suo
			// valore. Scelgo l'euristica se rand ha un valore compreso fra il
			// valore Prob del precedente e il suo.

			if (rand <= e0.getProb()) {

				nomeEuristica = "e0";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA START JOB------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e0.ordFix(model, istanza, q);

			} else if (rand > e0.getProb() && rand <= e1.getProb()) {

				nomeEuristica = "e1";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA SPEC JOB------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e1.ordFix(model, istanza, q);

			} else if (rand > e1.getProb() && rand <= e2.getProb()) {

				nomeEuristica = "e2";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA DURATA JOB------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e2.ordFix(model, istanza, q);

			} else if (rand > e2.getProb() && rand <= e3.getProb()) {

				nomeEuristica = "e3";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA QMIN------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e3.ordFix(model, istanza, q);

			} else if (rand > e3.getProb() && rand <= e4.getProb()) {

				nomeEuristica = "e4";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA QMAX------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e4.ordFix(model, istanza, q);

			} else if (rand > e4.getProb() && rand <= e5.getProb()) {

				nomeEuristica = "e5";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA FMIN------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e5.ordFix(model, istanza, q);

			} else if (rand > e5.getProb() && rand <= e6.getProb()) {

				nomeEuristica = "e6";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA FMAX------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e6.ordFix(model, istanza, q);

			} else if (rand > e6.getProb() && rand <= e7.getProb()) {

				nomeEuristica = "e7";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA UMIN------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e7.ordFix(model, istanza, q);

			} else if (rand > e7.getProb() && rand <= e8.getProb()) {

				nomeEuristica = "e8";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA UMAX------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e8.ordFix(model, istanza, q);

			} else if (rand > e8.getProb() && rand <= e9.getProb()) {

				nomeEuristica = "e9";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA START JOB DAY------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e9.ordFix(model, istanza, q);

			} else if (rand > e9.getProb() && rand <= 1) { // rand <=
															// e10.getProb()

				nomeEuristica = "e10";

				System.out.println(
						"---------------------------------------------------------------------------------------");
				System.out.println("--------------------------EURISTICA DURATA JOB 2------------------------");
				System.out.println(
						"---------------------------------------------------------------------------------------");
				e10.ordFix(model, istanza, q);

			}

			model.getModel().update();
			model.getModel().optimize();

			valueObj = model.getObjective();
			if (valueObj == 1.0E100)
				valueObj = 0.0;

			// Se � infeasible allora carico la soluzione precendente
			// Se � infeasible o se � feasible ma non � riuscito a trovare una
			// soluzione nel tempo definito allora carico la soluzione migliore

			if (model.getModel().get(GRB.IntAttr.Status) == 3) {

				segno = -1.5; // Negativo per 1.5, brutta soluzione

				System.out.println("*******************************************************************");
				System.out.println("       INFEASIBLE : CARICO LA MIGLIORE SOLUZIONE TROVATA");
				System.out.println("*******************************************************************");
				leggiSoluzione(model, "Best/00" + numeroIstanza + "/solution.sol");

			} else if (model.getModel().get(GRB.IntAttr.Status) == 9 && valueObj == 0.0) {

				segno = -2; // Negativo per due, brutta soluzione + perdita di
							// tempo

				System.out.println("*******************************************************************");
				System.out.println(" TIME LIMIT + NO SOLUTION : CARICO LA MIGLIORE SOLUZIONE TROVATA");
				System.out.println("*******************************************************************");
				leggiSoluzione(model, "Best/00" + numeroIstanza + "/solution.sol");

			} else {

				System.out.println("---------------------------------------------------------------------");
				System.out.println("Soluzione BEST:     " + bestObj);
				System.out.println("Soluzione CORRENTE: " + valueObj);

				segno = 1; // positivo
				
				// MOVE OR NOT
				if (valueObj < bestObj) {

					bestObj = valueObj;
					model.saveSol("Best/00" + numeroIstanza);
					// Salvo la soluzione corrente e proseguo con questa..
					model.saveSol("Current/00" + numeroIstanza);
					segno = 1; // positivo

				} else if (valueObj > bestObj) {

					segno = -1; // Negativo

					// valuto criterio di accettazione

					Accettazione criterio = new Accettazione();
					if (criterio.acceptanceProb(valueObj, bestObj, temp) > Math.random()) {

						System.out.println("---------------------------------------------------------------------");
						System.out.println("CRITERIO di ACCETTAZIONE -> Soluzione PEGGIORATIVA: " + valueObj);
						System.out.println("---------------------------------------------------------------------");

						peggiorativa = 1;
						// Salvo la soluzione corrente e proseguo con questa..
						model.saveSol("Current/00" + numeroIstanza);

					} else {

						// recupero la soluzione corrente precedente
						leggiSoluzione(model, "Current/00" + numeroIstanza + "/solution.sol");
						// leggiSoluzione(model, "Best/00" + numeroIstanza +
						// "/solution.sol");

						System.out.println("---------------------------------------------------------------------");
						System.out.println("Soluzione CORRENTE: " + valueObj);
						System.out.println("---------------------------------------------------------------------");

					}
				}
			}

			System.out.println("---------------------------------------------------------------------");
			System.out.println("Incremento o decremento dell'intevallo(%): " + segno * 0.15);
			System.out.println("---------------------------------------------------------------------");

			scriviLog(logFile, h, q, nomeEuristica, valueObj, peggiorativa);

			// Aggiorna valori Prob
			System.out.println("---------------------------------------------------------------------");
			probabilita.aggiornaPesi(nomeEuristica, segno * 0.15, logFile);
			System.out.println("---------------------------------------------------------------------");
		}
	}

	private void leggiSoluzione(ModelVar model, String soluzione) throws GRBException {

		model.restore();
		model.getModel().read(soluzione);
		// stopping rule: prima soluzione intera trovata
		model.getModel().getEnv().set(GRB.IntParam.SolutionLimit, 1);
		// Gurobi param
		model.getModel().getEnv().set(GRB.DoubleParam.Heuristics, 1);

		model.getModel().update();
		model.getModel().optimize();
	}

	private void scriviLog(FileWriter logFile, int h, double q, String nomeEuristica, Double valueObj, int peggiorativa)
			throws IOException {

		if (peggiorativa == 1) {
			logFile.write(h + "; " + q + "; " + nomeEuristica + "; " + valueObj + "; si; ");
		} else {
			logFile.write(h + "; " + q + "; " + nomeEuristica + "; " + valueObj + "; no; ");
		}
		logFile.flush();
	}
}
