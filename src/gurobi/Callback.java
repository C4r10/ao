/**
 *
 */
package gurobi;

/**
 * @author Carlo
 * 20 ott 2016
 *
 */

/* Copyright 2016, Gurobi Optimization, Inc. */

/*
  This example reads a model from a file, sets up a callback that
  monitors optimization progress and implements a custom
  termination strategy, and outputs progress information to the
  screen and to a log file.

  The termination strategy implemented in this callback stops the
  optimization of a MIP model once at least one of the following two
  conditions have been satisfied:
    1) The optimality gap is less than 10%
    2) At least 10000 nodes have been explored, and an integer feasible
       solution has been found.
  Note that termination is normally handled through Gurobi parameters
  (MIPGap, NodeLimit, etc.).  You should only use a callback for
  termination if the available parameters don't capture your desired
  termination criterion.
*/

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Callback extends GRBCallback {

	// Delimiter used in CSV file
	private static final String SPACE_DELIMITER = " ";
	private static final String NEW_LINE_SEPARATOR = "\n";

	// csv file header
	private static final String FILE_HEADER = "Name Value Obj = ";

	@SuppressWarnings("unused")
	private double lastiter;
	@SuppressWarnings("unused")
	private double lastnode;
	private GRBVar[] vars;
	@SuppressWarnings("unused")
	private FileWriter csvSol;

	public Callback(GRBVar[] xvars) {
		lastiter = lastnode = -GRB.INFINITY;
		vars = xvars;
	}

	protected void callback() {

		FileWriter csvSol;
		if (where == GRB.CB_MIPSOL) {
			try {
				// MIP solution callback
				@SuppressWarnings("unused")
				int nodecnt = (int) getDoubleInfo(GRB.CB_MIPSOL_NODCNT);
				double obj = getDoubleInfo(GRB.CB_MIPSOL_OBJ);
				int solcnt = getIntInfo(GRB.CB_MIPSOL_SOLCNT);
				double[] x = getSolution(vars);
				System.out.println("----------- New solution:obj " + obj + ", sol " + solcnt);
				csvSol = new FileWriter("soluz" + solcnt + ".sol");
				try {
					// for (int i=0; i<vars.length;i++)
					// {csvSol.write(vars[i].get(GRB.StringAttr.VarName)+"
					// "+x[i]+"\n");}
					// Write the CSV file header
					csvSol.append(FILE_HEADER + obj);
					csvSol.append(NEW_LINE_SEPARATOR);
				} catch (IOException ex) {
					Logger.getLogger(Callback.class.getName()).log(Level.SEVERE, null, ex);
				}

				for (int i = 0; i < vars.length; i++) {
					try {
						csvSol.write(vars[i].get(GRB.StringAttr.VarName) + SPACE_DELIMITER + x[i] + NEW_LINE_SEPARATOR);
					} catch (IOException ex) {
						Logger.getLogger(Callback.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
				csvSol.close();
			} catch (GRBException ex) {
				Logger.getLogger(Callback.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public double getObj(){
		double obj=.0;
		try {
			obj = getDoubleInfo(GRB.CB_MIPSOL_OBJ);
		} catch (GRBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Eccezione catch em all!!");
		}
		return obj;

	}	
}

