package gurobi;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CalendarioMedicoHtml {

	private final static String INTESTAZIONE = "<!DOCTYPE html><html lang=\"it\"><head>"
			+ "<link rel=\"stylesheet\" href=\"style/bootstrap.min.css\">"
			+ "<script type=\"text/javascript\" src=\"style/jquery-1.10.2.min.js\"></script>"
			+ "<script type=\"text/javascript\"> function showOrarioMedico(){ var t = document.getElementsByTagName(\"table\");"
			+ "for (var i = 0; i < t.length; i++) { t[i].style.display = \"none\"; } "
			+ "document.getElementById($('select[name=myselect]').val()).style.display = \"\"; } </script> "
			+ "<style> .container { padding-right: 0px; padding-left: 0px; margin-right: 100px; margin-left: 100px; } </style> </head>";
	private final static String INTESTAZIONE_Global = "<!DOCTYPE html><html lang=\"it\"><head>"
			+ "<link rel=\"stylesheet\" href=\"../../../style/bootstrap.min.css\">"
			+ "<script type=\"text/javascript\" src=\"../../../style/jquery-1.10.2.min.js\"></script>" + "</head>";
	private final static String BODY_B = "<body style=\"font-size:12px\"> <div class=\"container\">";
	private final static String INIT_SELECT = " <br> \t<div class=\"main\">\n" + "\t\t<form>\n"
			+ "\t\t\t<select name=\"myselect\" onchange=\"showOrarioMedico()\">\n";
	private final static String FINE_SELECT = "\t\t\t</select>\n" + "\t\t</form>\n" + "\n";
	private final static String INIT_SELECT_Global = "\t<div class=\"main\">\n" + "\t\t<form>\n"
			+ "\t\t\t<select id=\"myselect\" onchange=\"showOrarioMedico()\">\n"
			+ "\t\t\t\t<option>Seleziona Medico</option>\n" + "\t\t\t</select>\n" + "\t\t</form>\n" + "\n <br><br>";
	private final static String BODY_E = "</body>";
	private final static String TAB_BEGIN = "<table border = \"1\">";
	private final static String TAB_ROW_B_FIRST = "<tr class = \"first\">";
	private final static String TAB_ROW_B_EVEN = "<tr class = \"even\">";
	private final static String TAB_ROW_B_ODD = "<tr class = \"odd\">";
	private final static String TAB_CEL_B = "<td>";
	private final static String TAB_CEL_B_JOB = "<td style=\"background-color:yellow\">";
	private final static String TAB_CEL_B_DAY = "<td id=\"days\">";
	private final static String TAB_ROW_E = "</tr>";
	private final static String TAB_CEL_E = "</td>";
	private final static String TAB_END = "</table>";

	public static Lettore inst;
	public static GRBEnv env;
	public static GRBModel model;
	public static FileOutputStream prova;
	public static PrintStream scrivi;

	public static void main(String[] args) throws IOException, GRBException {

		/*
		 * PER CAMBIARE OUTPUT � NECESSARIO CAMBIARE: SOLUZIONE DI RIFERIMENTO
		 * MODELLO E QUINDI ISTANZA DI RIFERIMENTO
		 * 
		 * IL FILE GENERATO RILEVA IN AUTOMATICO IL NUMERO DI MEDICI.
		 */
		Date date = new Date();

		String indice = "2";

		inst = new Lettore("istanze/inst" + indice + ".txt");
		env = new GRBEnv("GUROBI.log");

		model = new GRBModel(env, "Modelli/00" + indice + "/modello_00" + indice + "_PLI.mps");
		model.read("Best/00" + indice + "/solution.sol");

		model.getEnv().set(GRB.IntParam.SolutionLimit, 1);
		model.getEnv().set(GRB.IntParam.Presolve, 0); // valore di default
		model.getEnv().set(GRB.DoubleParam.Heuristics, 1);

		model.update();
		model.optimize();

		prova = new FileOutputStream(
				"tabelle-orari/istanza_"+indice+"_orari-medico" + new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss").format(date) + ".html");
		scrivi = new PrintStream(prova);

		scrivi.println(INTESTAZIONE);
		scrivi.println(BODY_B);
		scrivi.println("<p>NUM. MEDICI</p>" + "<input id=\"numero_medici\" type=\"int\" value=" + inst.getN_physicians()
				+ " readonly>");
		scrivi.println(INIT_SELECT);
		for (int p = 0; p < inst.getN_physicians(); p++) {
			scrivi.println("\t\t\t\t<option>" + p + "</option>\n");

		}
		scrivi.println(FINE_SELECT);
		for (int p = 0; p < inst.getN_physicians(); p++) {
			generaTabellaMedico(p, model, inst, scrivi);

		}
		scrivi.println(BODY_E);
	}

	private static void generaTabellaMedico(int p, GRBModel model, Lettore inst, PrintStream scrivi)
			throws GRBException, FileNotFoundException {

		scrivi.println("<table id=\"" + p + "\" class=\"orari\" border = \"1\" style=\"display:none\">\n"); // inizio
																											// tabella
		scrivi.println(TAB_ROW_B_FIRST);
		scrivi.println(TAB_CEL_B);
		scrivi.println(TAB_CEL_E);
		for (int t = 0; t < inst.getN_timeslots(); t++) {
			scrivi.println(TAB_CEL_B);
			scrivi.println(t / 2 + ":" + (t % 2 == 0 ? "00" : "30"));
			scrivi.println(TAB_CEL_E);
		}
		scrivi.println(TAB_ROW_E);

		for (int d = 0; d < inst.getN_days(); d++) {
			scrivi.println(d % 2 == 0 ? TAB_ROW_B_EVEN : TAB_ROW_B_ODD);

			scrivi.println(TAB_CEL_B_DAY);
			scrivi.println("giorno: " + d);
			scrivi.println(TAB_CEL_E);
			for (int t = 1; t <= inst.getN_timeslots(); t++) {

				if (t >= model.getVarByName("zb" + p + "-" + d).get(GRB.DoubleAttr.X)
						&& t < model.getVarByName("ze" + p + "-" + d).get(GRB.DoubleAttr.X)) {

					scrivi.println(TAB_CEL_B_JOB);

				} else
					scrivi.println(TAB_CEL_B);

				for (int j = 0; j < inst.getN_jobs(); j++) {
					double sum = 0;
					for (int delta = 0; delta < inst.getMatrix_d()[j][d]; delta++) {
						if (t - 1 - delta >= 0)
							sum += model.getVarByName("y" + p + "-" + j + "-" + d + "-" + (t - 1 - delta))
									.get(GRB.DoubleAttr.X);
					}
					if (sum > 0)
						scrivi.print(j);
				}

				scrivi.println(TAB_CEL_E);
			}
			scrivi.println(TAB_ROW_E);
		}
		scrivi.println(TAB_END);

	}

	public static void generaTabellaDaModello(GRBModel model, String path, Lettore inst)
			throws FileNotFoundException, GRBException {
		FileOutputStream out = new FileOutputStream(path + "/tabella-orari.html");
		PrintStream scrivi = new PrintStream(out);
		scrivi.println(INTESTAZIONE_Global);
		scrivi.println(BODY_B);
		scrivi.println("<p>NUM. MEDICI</p>" + "<input id=\"numero_medici\" type=\"int\" value=" + inst.getN_physicians()
				+ " readonly>");
		scrivi.println(INIT_SELECT_Global);

		for (int p = 0; p < inst.getN_physicians(); p++) {

			generaTabellaMedico(p, model, inst, scrivi);

		}
		scrivi.println(BODY_E);
	}

}
