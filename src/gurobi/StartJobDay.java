package gurobi;

import java.util.Arrays;
import java.util.Vector;

public class StartJobDay implements Euristica {

	private double prob=0.0909091;
	private double intervallo=0.0909091;

	@SuppressWarnings("unchecked")
	@Override
	public void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException {
		// TODO Auto-generated method stub

		Struttura[][] medici_gg = new Struttura[inst.getN_physicians()][inst.getN_days()];

		int[][] matriceD = inst.getMatrix_d();
		Vector<Vector<Vector<Integer>>> TJ = inst.getTJ();

		GRBVar[][] ze = model.getZe();
		GRBVar[][] zb = model.getZb();

		double tot = 0;
		double durata = 0;
		double jobs = 0;
		double gg = 0;

		for (int p = 0; p < inst.getN_physicians(); p++) {

			double costo = 0;

			for (int d = 0; d < inst.getN_days(); d++) {

				Struttura uno = new Struttura(d);
				medici_gg[p][d] = uno;

				double Ze = ze[p][d].get(GRB.DoubleAttr.X);
				double Zb = zb[p][d].get(GRB.DoubleAttr.X);

				costo += Ze - Zb;
				gg = Ze - Zb;
				durata += Ze - Zb;
				// Time slot che il medico esame sta in ospedale senza lavorare
				for (int j = 0; j < inst.getN_jobs(); j++) {
					costo -= model.getXVal(p, j, d) * matriceD[j][d];
					jobs += model.getXVal(p, j, d) * matriceD[j][d];
					gg -= model.getXVal(p, j, d) * matriceD[j][d];
				}
				uno.setCosto(gg);
			}
			tot += costo;
		}

//		System.out.println("Totale: " + tot + "---------------------");
//		System.out.println("Durata: " + durata + "---------------------");
//		System.out.println("Job: " + jobs + "---------------------");

		for (int p = 0; p < inst.getN_physicians(); p++) {
			Arrays.sort(medici_gg[p], new Ordina());
		}

		// Fisso le variabili tranne le y
		for (int p = 0; p < inst.getN_physicians(); p++) {
			if (medici_gg[p][0].getCosto() < 10) {
				for (int d = medici_gg[p].length - 1; d > medici_gg[p].length * (1 - percent); d--) {

					int dd = medici_gg[p][d].getIndice();
					model.setW(p, dd);

					for (int j = 0; j < inst.getN_jobs(); j++) {
						model.setX(p, j, dd);
					}
				}
			}
		}

		// Definisci i vincoli riguardanti i lavori
		for (int p = 0; p < inst.getN_physicians(); p++) {
			//for (int d = 0; d < medici_gg[p].length * (percent); d++) {
				int dd = medici_gg[p][0].getIndice();

				for (int j = 0; j < inst.getN_jobs(); j++) {
					for (int t = 0; t < inst.getN_timeslots(); t++) {
						int n = TJ.get(j).get(dd).size();

						if (model.getYVal(p, j, dd, t) == 1 && n > 1 && medici_gg[p][0].getCosto() >= 10) {
							model.addConsStartJob(p, j, dd, t);

						}
					}
				}
			//}
		}
	}

	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	public double getIntervallo() {
		// TODO Auto-generated method stub
		return intervallo;
	}

	public void setIntervallo(double intervallo) {
		this.intervallo = intervallo;
	}
}
