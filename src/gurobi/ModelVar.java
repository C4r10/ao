/**
 *
 */
package gurobi;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Carlo 20 ott 2016
 *
 */
public class ModelVar {

	private GRBModel model;
	private Lettore inst;

	private GRBVar[][][][] y;
	private GRBVar[][][] x;
	private GRBVar[][] w;
	private GRBVar[][] zb;
	private GRBVar[][] ze;

	private Vector<GRBVar> fixedVars;
	private Vector<GRBConstr> newConstr;

	private static final String SPACE_DELIMITER = " ";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final String FILE_HEADER = "Name Value Obj = ";

	/**
	 * Costruttore
	 */
	public ModelVar(GRBModel _model, Lettore _inst) throws GRBException {
		model = _model;
		inst = _inst;

		this.fixedVars = new Vector<>();
		this.newConstr = new Vector<>();

		y = new GRBVar[inst.getN_physicians()][inst.getN_jobs()][inst.getN_days()][inst.getN_timeslots()];
		x = new GRBVar[inst.getN_physicians()][inst.getN_jobs()][inst.getN_days()];
		w = new GRBVar[inst.getN_physicians()][inst.getN_days()];
		zb = new GRBVar[inst.getN_physicians()][inst.getN_days()];
		ze = new GRBVar[inst.getN_physicians()][inst.getN_days()];

		for (int p = 0; p < inst.getN_physicians(); p++)
			for (int d = 0; d < inst.getN_days(); d++) {
				w[p][d] = model.getVarByName("w" + p + "-" + d);
				zb[p][d] = model.getVarByName("zb" + p + "-" + d);
				ze[p][d] = model.getVarByName("ze" + p + "-" + d);
				for (int j = 0; j < inst.getN_jobs(); j++) {
					x[p][j][d] = model.getVarByName("x" + p + "-" + j + "-" + d);
					for (int t = 0; t < inst.getN_timeslots(); t++) {
						y[p][j][d][t] = model.getVarByName("y" + p + "-" + j + "-" + d + "-" + t);
					}
				}
			}

		model.update();
	}

	public double getObjective() {
		try {
			return model.get(GRB.DoubleAttr.ObjVal);
		} catch (GRBException e) {
			// TODO Auto-generated catch block
			return 0.0;
		}
	}

	/**
	 * @return the w
	 */
	public GRBVar[][] getW() {
		return w;
	}

	/**
	 * @return the w val
	 */
	public double getWVal(int p, int d) throws GRBException {
		return w[p][d].get(GRB.DoubleAttr.X);
	}

	/**
	 * @param w
	 *            the w to set
	 */
	public void setW(int p, int d) throws GRBException {
		w[p][d].set(GRB.DoubleAttr.LB, getWVal(p, d));
		w[p][d].set(GRB.DoubleAttr.UB, getWVal(p, d));
		fixedVars.add(w[p][d]);
	}

	/**
	 * @return the x
	 */
	public GRBVar[][][] getX() {
		return x;
	}

	/**
	 * @return the x val
	 */
	public double getXVal(int p, int j, int d) throws GRBException {
		return x[p][j][d].get(GRB.DoubleAttr.X);
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int p, int j, int d) throws GRBException {
		x[p][j][d].set(GRB.DoubleAttr.LB, getXVal(p, j, d));
		x[p][j][d].set(GRB.DoubleAttr.UB, getXVal(p, j, d));
		fixedVars.add(x[p][j][d]);
	}

	/**
	 * @return the y
	 */
	public GRBVar[][][][] getY() {
		return y;
	}

	/**
	 * @return the y
	 */
	public double getYVal(int p, int j, int d, int t) throws GRBException {
		return y[p][j][d][t].get(GRB.DoubleAttr.X);
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int p, int j, int d, int t) throws GRBException {
		y[p][j][d][t].set(GRB.DoubleAttr.LB, getYVal(p, j, d, t));
		y[p][j][d][t].set(GRB.DoubleAttr.UB, getYVal(p, j, d, t));
		fixedVars.add(y[p][j][d][t]);
	}

	/**
	 * @return the zb
	 */
	public GRBVar[][] getZb() {
		return zb;
	}

	/**
	 * @return the zb val
	 */
	public double getZBVal(int p, int d) throws GRBException {
		return zb[p][d].get(GRB.DoubleAttr.X);
	}

	/**
	 * @param zb
	 *            the zb to set
	 */
	public void setZB(int p, int d) throws GRBException {
		zb[p][d].set(GRB.DoubleAttr.LB, getZBVal(p, d));
		zb[p][d].set(GRB.DoubleAttr.UB, getZBVal(p, d));
		fixedVars.add(zb[p][d]);
	}

	/**
	 * @return the ze
	 */
	public GRBVar[][] getZe() {
		return ze;
	}

	/**
	 * @return the ze
	 */
	public double getZEVal(int p, int d) throws GRBException {
		return ze[p][d].get(GRB.DoubleAttr.X);
	}

	/**
	 * @param ze
	 *            the ze to set
	 */
	public void setZE(int p, int d) throws GRBException {
		ze[p][d].set(GRB.DoubleAttr.LB, getZEVal(p, d));
		ze[p][d].set(GRB.DoubleAttr.UB, getZEVal(p, d));
		fixedVars.add(ze[p][d]);
	}

	/**
	 * @return the model
	 */
	public GRBModel getModel() {
		return model;
	}

	public void restore() throws GRBException {
		GRBConstr constr;
		GRBVar var;
		while (fixedVars.size() > 0) {
			var = fixedVars.remove(0);
			// System.out.println("****************** "+var+" ******************
			// ");
			var.set(GRB.DoubleAttr.LB, 0.0);
			var.set(GRB.DoubleAttr.UB, 1.0);
		}

		while (newConstr.size() > 0) {
			constr = newConstr.remove(0);
			// System.out.println("****************** "+constr+"
			// ****************** ");
			model.remove(constr);
		}
	}

	public void saveSol(String cartella) throws GRBException, IOException {

		FileWriter csvSol = new FileWriter( cartella + "/solution.sol");

		// Write the CSV file header
		csvSol.append(FILE_HEADER + ((GRBLinExpr) model.getObjective()).getValue());
		csvSol.append(NEW_LINE_SEPARATOR);

		GRBVar[] vars = model.getVars();
		for (int i = 0; i < vars.length; i++) {

			csvSol.write(vars[i].get(GRB.StringAttr.VarName) + SPACE_DELIMITER + vars[i].get(GRB.DoubleAttr.X)
					+ NEW_LINE_SEPARATOR);

		}
		csvSol.close();
	}

	/**
	 * @param p
	 * @throws GRBException
	 *             fissa le GRBVar legate al medico di indice p
	 */
	public void setMedico(int p) throws GRBException {
		for (int d = 0; d < inst.getN_days(); d++) {
			setW(p, d);
			// setZB(p, d);
			// setZE(p, d);
			for (int j = 0; j < inst.getN_jobs(); j++) {
				setX(p, j, d);
			}
		}
	}

	public void addConsStartJob(int p, int j, int d, int t) throws GRBException {

		System.out.println("Il medico " + p + " non inizia il lavoro " + j + " nel t-slot " + t + " del giorno " + d);

		GRBConstr constr = (model.addConstr(y[p][j][d][t], GRB.EQUAL, 0,
				"C_NEW" + "_" + p + "_" + j + "_" + d + "_" + t));
		// System.out.println("****************** "+constr+" ******************
		// ");
		newConstr.add(constr);

	}

	public void addConsSpecJob(int p, int j, int d) throws GRBException {

		System.out.println("Il medico " + p + " non inizia il lavoro " + j + " nel giorno " + d);

		GRBConstr constr = (model.addConstr(x[p][j][d], GRB.EQUAL, 0, "C_NEW" + "_" + p + "_" + j + "_" + d));
		// System.out.println("****************** "+constr+" ******************
		// ");
		newConstr.add(constr);

	}

	public void addCons(GRBLinExpr expr, int n) throws GRBException {

		GRBConstr constr = model.addConstr(expr, GRB.LESS_EQUAL, n, "C_NEW");
		// System.out.println("****************** "+constr+" ******************
		// ");
		newConstr.add(constr);

	}
}
