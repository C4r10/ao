/**
 *
 */
package gurobi;

import java.util.Comparator;

/**
 * @author Carlo 21 ott 2016
 *
 */
public class Ordina implements Comparator {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object arg0, Object arg1) {
		Struttura p1 = (Struttura) arg0;
		Struttura p2 = (Struttura) arg1;

		int ret = -1;
		// business logic here
		if (p1.costo == p2.costo) {
			ret = 0;
		} else if (p1.costo > p2.costo) {
			ret = -1;
		} else if (p1.costo < p2.costo) {
			ret = 1;
		} // end business logic
		return ret;
	}
}
