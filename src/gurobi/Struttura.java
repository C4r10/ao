/**
 *
 */
package gurobi;

/**
 * @author Carlo
 * 20 ott 2016
 *
 */
public class Struttura {
	double costo;
	int indice;
	
	/**
	 * costruttore
	 */
	public Struttura(int _indice) {
		// TODO Auto-generated constructor stub
		indice=_indice;
		costo=Double.NaN;		
	}
	
	/**
	 * @param costo the costo to set
	 */
	public void setCosto(double costo) {
		this.costo = costo;
	}
	
	public double getCosto() {
		return costo;
	}
	
	/**
	 * @return the indice
	 */
	public int getIndice() {
		return indice;
	}
}
