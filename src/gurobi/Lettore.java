package gurobi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class Lettore {
	
	FileReader f;
	
	private int n_days;
	private int n_weeks;
	private int n_timeslots;
	private int n_physicians;
	private int n_jobs;
	private int n_roles;
	private int nmax_shifts;
	
	private int[][] PJ;
	private int[][] PR;
	private Vector<Vector<Vector<Integer>>> TJ;
	private int[][] matrix_d;
	private int[][][] H;
	private int[] A;
	private int[][] LMIN;
	private int[][] LMAX;
	private int[] SH; 
	private int[][] ISH;
	private int[][] dSH;
	private int[][] QMAX;
	private int[][] QMIN;
	private int[] FMAX;
	private int[] FMIN;
	private int[] UMIN;
	private int[] UMAX;

	public Lettore(String name_file) throws IOException{
		f = new FileReader(name_file);
		fileParser();
	}
	
	public void fileParser() throws IOException{

		BufferedReader b;
		b=new BufferedReader(f);
		
		String s;
		int nrow = 1;
				
		while(true) {
		  s=b.readLine();
		  
		  if(s == null){
			b.close();
		    break;
		  }
		  
		  if(nrow < 8){
			  String[] row = s.split("=");
			  int constant = Integer.parseInt(row[1]);
			  if(row[0].equals("N_days"))
				  n_days = constant;
			  if(row[0].equals("N_timeslots"))
				  n_timeslots = constant;
			  if(row[0].equals("N_physicians"))
				  n_physicians = constant;
			  if(row[0].equals("N_jobs"))
				  n_jobs = constant;
			  if(row[0].equals("N_roles"))
				  n_roles = constant;  
			  if(row[0].equals("N_weeks"))
				  n_weeks = constant;
			  if(row[0].equals("Nmax_shifts"))
				  nmax_shifts = constant;
		  }
		  else{
			  if(nrow == 8){
				  
				  PJ = new int[n_physicians][n_jobs];
				  PR = new int[n_physicians][n_roles];
				  TJ = new Vector<Vector<Vector<Integer>>>();
					for(int j=0; j<n_jobs; j++){
						TJ.add(new Vector<Vector<Integer>>());
						for(int d=0; d<n_days; d++)
							TJ.get(j).add(new Vector<Integer>());
					}
				  matrix_d = new int[n_jobs][n_days];
				  H = new int[n_roles][n_jobs][n_days];
				  SH = new int[n_jobs];
				  ISH = new int[n_jobs][nmax_shifts];
				  dSH = new int[n_jobs][nmax_shifts];
				  QMAX = new int[n_physicians][n_days];
				  QMIN = new int[n_physicians][n_days];
				  FMAX = new int[n_physicians];
				  FMIN = new int[n_physicians];
				  A = new int[n_jobs];
				  LMAX = new int[n_physicians][n_jobs];
				  LMIN = new int[n_physicians][n_jobs];
				  UMIN = new int[n_physicians];
				  UMAX = new int[n_physicians];
				  
			  }
			  
			  String[] heading = s.split("-");
			  
			  if(heading.length > 0){
				  
				  if(heading[0].equals("MATRIX_PJ")){
					  for(int p=0; p<n_physicians; p++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int j=0; j<n_jobs; j++)
							  PJ[p][j] = Integer.parseInt(row[j]);
					  }
				  }
				  
				  if(heading[0].equals("MATRIX_PR")){
					  for(int p=0; p<n_physicians; p++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int r=0; r<n_roles; r++){
							  PR[p][r] = Integer.parseInt(row[r]);
						  }
					  }
				  }
				  
				 if(heading[0].equals("MATRIX TJ")){
					  for(int j=0; j<n_jobs; j++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int d=0; d<n_days; d++){
							  String list = row[d].split("\\{")[1].split("\\}")[0];
							  String[] numbers = list.split(",");
							  for(int dd=0; dd<numbers.length; dd++)
								  TJ.get(j).get(d).add(Integer.parseInt(numbers[dd]));
						  }
					  }
				  }
				  
				  if(heading[0].equals("MATRIX_d")){
					  for(int j=0; j<n_jobs; j++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int d=0; d<n_days; d++){
							  matrix_d[j][d] = Integer.parseInt(row[d]);
						  }
					  }
				  }
				  
				  if(heading[0].equals("MATRIX_H")){
					  int rnumber = Integer.parseInt(heading[1].split("]")[0].split("=")[1]);
					  for(int j=0; j<n_jobs; j++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int d=0; d<n_days; d++){
							  H[rnumber-1][j][d] = Integer.parseInt(row[d]);
						  }
					  }
				  }
				  
				  if(heading[0].equals("TMATRIX_A")){
					  s=b.readLine();
					  String[] row = s.split("-");
					  for(int j=0; j<n_jobs; j++)
						  A[j] = Integer.parseInt(row[j]);
				  }
				  
				  if(heading[0].equals("MATRIX_LMAX")){
					  for(int p=0; p<n_physicians; p++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int j=0; j<n_jobs; j++){
							  LMAX[p][j] = Integer.parseInt(row[j]);
						  }
					  }
				  }
				  
				  if(heading[0].equals("MATRIX_LMIN")){
					  for(int p=0; p<n_physicians; p++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int j=0; j<n_jobs; j++){
							  LMIN[p][j] = Integer.parseInt(row[j]);
						  }
					  }
				  }
				  
				  if(heading[0].equals("TMATRIX_SH")){
					  s=b.readLine();
					  String[] row = s.split("-");
					  for(int j=0; j<n_jobs; j++)
						  SH[j] = Integer.parseInt(row[j]);
				  }
				  
				  if(heading[0].equals("MATRIX_ISH")){
					  for(int j=0; j<n_jobs; j++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int h=0; h<row.length; h++)
							  ISH[j][h] = Integer.parseInt(row[h]);
					  }
				  }
				  
				  if(heading[0].equals("MATRIX_dSH")){
					  for(int j=0; j<n_jobs; j++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int h=0; h<row.length; h++)
							  dSH[j][h] = Integer.parseInt(row[h]);
					  }
				  }
				  
				  if(heading[0].equals("MATRIX_QMAX")){
					  for(int p=0; p<n_physicians; p++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int d=0; d<n_days; d++)
							  QMAX[p][d] = Integer.parseInt(row[d]);
					  }  
				  }
				  
				  if(heading[0].equals("MATRIX_QMIN")){
					  for(int p=0; p<n_physicians; p++){
						  s=b.readLine();
						  String[] row = s.split("-");
						  for(int d=0; d<n_days; d++)
							  QMIN[p][d] = Integer.parseInt(row[d]);
					  }  
				  }
				  
				  if(heading[0].equals("TMATRIX_FMAX")){
					  s=b.readLine();
					  String[] row = s.split("-");
					  for(int p=0; p<n_physicians; p++)
						  FMAX[p] = Integer.parseInt(row[p]);
				  }
				  
				  if(heading[0].equals("TMATRIX_FMIN")){
					  s=b.readLine();
					  String[] row = s.split("-");
					  for(int p=0; p<n_physicians; p++)
						  FMIN[p] = Integer.parseInt(row[p]);
				  }
				  
				  if(heading[0].equals("TMATRIX_UMAX")){
					  s = b.readLine();
					  String[] row = s.split("-");
					  for(int p=0; p<n_physicians; p++){
						  UMAX[p] = Integer.parseInt(row[p]);
					  }
				  }
				  
				  if(heading[0].equals("TMATRIX_UMIN")){
					  s = b.readLine();
					  String[] row = s.split("-");
					  for(int p=0; p<n_physicians; p++){
						  UMIN[p] = Integer.parseInt(row[p]);
					  }
				  }
				  
			  }
			  
		  }
	
		  nrow++;
	    }
	}

	public int getN_days() {
		return n_days;
	}

	public int getN_weeks() {
		return n_weeks;
	}

	public int getN_timeslots() {
		return n_timeslots;
	}

	public int getN_physicians() {
		return n_physicians;
	}

	public int getN_jobs() {
		return n_jobs;
	}

	public int getN_roles() {
		return n_roles;
	}

	public int getNmax_shifts() {
		return nmax_shifts;
	}

	public int[][] getPJ() {
		return PJ;
	}

	public int[][] getPR() {
		return PR;
	}

	public Vector<Vector<Vector<Integer>>> getTJ() {
		return TJ;
	}

	public int[][] getMatrix_d() {
		return matrix_d;
	}

	public int[][][] getH() {
		return H;
	}

	public int[] getA() {
		return A;
	}

	public int[][] getLMIN() {
		return LMIN;
	}

	public int[][] getLMAX() {
		return LMAX;
	}

	public int[] getSH() {
		return SH;
	}

	public int[][] getISH() {
		return ISH;
	}

	public int[][] getdSH() {
		return dSH;
	}

	public int[][] getQMAX() {
		return QMAX;
	}

	public int[][] getQMIN() {
		return QMIN;
	}

	public int[] getFMAX() {
		return FMAX;
	}

	public int[] getFMIN() {
		return FMIN;
	}

	public int[] getUMIN() {
		return UMIN;
	}

	public int[] getUMAX() {
		return UMAX;
	}

	
	
	
}