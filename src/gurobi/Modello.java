
package gurobi; 

import java.io.IOException;
import java.util.Vector;

public class Modello {
    public static boolean addWpdToObjective = false;

    public static void creaModello(Lettore inst, String dest_file) throws IOException {

        try {
            GRBEnv env = new GRBEnv();
            GRBModel model = new GRBModel(env);
            int[][] PJ = inst.getPJ();
            int[][] PR = inst.getPR();
            int[][][] H = inst.getH();
            int[][] d_mat = inst.getMatrix_d();
            int[] A = inst.getA();
            int Q = inst.getN_timeslots();
            Vector<Vector<Vector<Integer>>> TJ = inst.getTJ();

            // DEFINIZIONE DELLE VARIABILI

            GRBVar[][][] x = new GRBVar[inst.getN_physicians()][inst.getN_jobs()][inst.getN_days()];

            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int j = 0; j < inst.getN_jobs(); j++)
                    for (int d = 0; d < inst.getN_days(); d++)
                        x[p][j][d] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x" + p + "-" + j + "-" + d);

            GRBVar[][][][] y = new GRBVar[inst.getN_physicians()][inst.getN_jobs()][inst.getN_days()][inst.getN_timeslots()];

            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int j = 0; j < inst.getN_jobs(); j++)
                    for (int d = 0; d < inst.getN_days(); d++)
                        for (int t = 0; t < inst.getN_timeslots(); t++)
                            y[p][j][d][t] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "y" + p + "-" + j + "-" + d + "-" + t);

            GRBVar[][] w = new GRBVar[inst.getN_physicians()][inst.getN_days()];
            GRBVar[][] zb = new GRBVar[inst.getN_physicians()][inst.getN_days()];
            GRBVar[][] ze = new GRBVar[inst.getN_physicians()][inst.getN_days()];

            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int d = 0; d < inst.getN_days(); d++) {
                    w[p][d] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "w" + p + "-" + d);
                    zb[p][d] = model.addVar(1.0, Q, 0.0, GRB.INTEGER, "zb" + p + "-" + d);
                    ze[p][d] = model.addVar(1.0, Q+1.0, 0.0, GRB.INTEGER, "ze" + p + "-" + d);
                }


            model.update();


            // DEFINIZIONE DEI VINCOLI

            GRBLinExpr target = new GRBLinExpr();
            
            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int d = 0; d < inst.getN_days(); d++) {
                    target.addTerm(1.0, ze[p][d]);
                    target.addTerm(-1.0, zb[p][d]);
                    if (addWpdToObjective) {
                        target.addTerm(1.0, w[p][d]);
                    }
                }

            model.setObjective(target, GRB.MINIMIZE);


            GRBLinExpr expr;
            // aggiungo vincoli (2)
            for (int r = 0; r < inst.getN_roles(); r++) {
                for (int j = 0; j < inst.getN_jobs(); j++) {
                    for (int d = 0; d < inst.getN_days(); d++) {
                        if (d_mat[j][d] > 0) { //controllo che un job abbia durata nel giorno d
                            expr = new GRBLinExpr();

                            for (int p = 0; p < inst.getN_physicians(); p++) {
                                if (PJ[p][j] > 0 && PR[p][r] > 0) {
                                    expr.addTerm(1.0, x[p][j][d]);
                                }
                            }

                            model.addConstr(expr, GRB.EQUAL, H[r][j][d], constrName("c2 (" + H[r][j][d] + " medici di ruolo " + r + " devono fare job " + j + " il giorno " + d + ")"));
                        }
                    }
                }
            }

            // aggiungo vincoli (3)
            for (int j = 0; j < inst.getN_jobs(); j++)
                for (int d = 0; d < inst.getN_days(); d++)
                    //for (int t = 0; t < inst.getN_timeslots(); t++)
                    if (TJ.get(j).get(d).get(0) != 0) for (int t : TJ.get(j).get(d)) {
                        for (int p = 0; p < inst.getN_physicians(); p++) {
                            if (PJ[p][j] > 0) {
                                expr = new GRBLinExpr();

                                for (int k = 0; k < inst.getN_physicians(); k++) {
                                    if (PJ[k][j] > 0) {
                                        expr.addTerm(1.0, y[k][j][d][t-1]);
                                    }
                                }
                                int Hsum = 0;
                                for (int r = 0; r < inst.getN_roles(); r++) {
                                    Hsum = Hsum + H[r][j][d];
                                }
                                expr.addTerm(-Hsum, y[p][j][d][t-1]);

                                model.addConstr(expr, GRB.GREATER_EQUAL, 0.0, constrName("c3 (job: " + j + " giorno: " + d + " timeslot: " + t + " medico: " + p + ")"));
                            }
                        }
                    }


            // aggiungo vincoli (4)
            for (int j = 0; j < inst.getN_jobs(); j++)
                for (int d = 0; d < inst.getN_days(); d++)
                    for (int p = 0; p < inst.getN_physicians(); p++) {
                        if (PJ[p][j] > 0) {
                            expr = new GRBLinExpr();

                            if (TJ.get(j).get(d).get(0) != 0) {
                                for (int t : TJ.get(j).get(d)) {
                                    expr.addTerm(1.0, y[p][j][d][t-1]);
                                }

                                expr.addTerm(-1.0, x[p][j][d]);

                                model.addConstr(expr, GRB.EQUAL, 0.00, constrName("c4 (se medico " + p + " fa job " + j + " il giorno " + d + " allora x" + p + "-" + j + "-" + d + "=1)"));
                            }
                        }
                    }


            // aggiungo vincoli (5)
            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int d = 0; d < inst.getN_days(); d++)
                    for (int t = 0; t < inst.getN_timeslots(); t++) {
                        expr = new GRBLinExpr();

                        for (int j = 0; j < inst.getN_jobs(); j++) {
                            int i = t - d_mat[j][d] + 1;
                            if (i < 0) i = 0;
                            for (; i <= t; i++) {
                                expr.addTerm(1.0, y[p][j][d][i]);
                            }
                        }

                        model.addConstr(expr, GRB.LESS_EQUAL, 1.0, constrName("c5 (no sovrapposizioni di job per il medico " + p + " all'istante " + t + " del giorno " + d));
                    }


            // aggiungo vincoli (6)(7)(8)
            for (int d = 0; d < inst.getN_days(); d++) {
                for (int j = 0; j < inst.getN_jobs(); j++) {
                    if (TJ.get(j).get(d).get(0) != 0) for (int t : TJ.get(j).get(d)) {
                        for (int p = 0; p < inst.getN_physicians(); p++) {
                            if (PJ[p][j] > 0) {
                                // vincolo (6)
                                expr = new GRBLinExpr();

                                int li = t + d_mat[j][d];
                                for (int l = li; l <= li + A[j] - 1; l++) {
                                    for (int k = 0; k < inst.getN_jobs(); k++) {
                                        if (k != j) {
                                            expr.addTerm(1.0, y[p][k][d][l-1]); //l-1 perch� i timeslot partono da 1 e gli indici da 0
                                        }
                                    }
                                }
                                expr.addTerm(A[j], y[p][j][d][t-1]);

                                model.addConstr(expr, GRB.LESS_EQUAL, A[j], constrName("c6 (dopo il job " + j + " nel giorno " + d + " e all'istante " + t + " il medico " + p + " deve riposare"));

                                // vincolo (7)
                                expr = new GRBLinExpr();
                                expr.addTerm(1.0, zb[p][d]);
                                expr.addTerm(Q - t, y[p][j][d][t-1]);
                                model.addConstr(expr, GRB.LESS_EQUAL, Q, constrName("c7 (collego zb a y, job: " + j + " giorno: " + d + " timeslot: " + t + " medico: " + p));

                                // vincolo (8)
                                expr = new GRBLinExpr();
                                expr.addTerm(1.0, ze[p][d]);
                                expr.addTerm(-t - d_mat[j][d], y[p][j][d][t-1]);
                                model.addConstr(expr, GRB.GREATER_EQUAL, 0.0, constrName("c8 (collego ze a y, job: " + j + " giorno: " + d + " timeslot: " + t + " medico: " + p));
                            }
                        }
                    }
                }
            }


            // aggiungo vincoli (9)
            for (int p = 0; p < inst.getN_physicians(); p++) {
                for (int d = 0; d < inst.getN_days(); d++) {

                    expr = new GRBLinExpr();
                    expr.addTerm(1, ze[p][d]);
                    expr.addTerm(-1, zb[p][d]);
                    expr.addTerm(-inst.getQMAX()[p][d], w[p][d]);
                    model.addConstr(expr, GRB.LESS_EQUAL, 0, constrName("c09a: ze" + p + "-" + d + "-zb" + p + "-" + d + " <= q.w" + p + "-" + d));

                    expr = new GRBLinExpr();
                    expr.addTerm(1, ze[p][d]);
                    expr.addTerm(-1, zb[p][d]);
                    expr.addTerm(-inst.getQMIN()[p][d], w[p][d]);
                    model.addConstr(expr, GRB.GREATER_EQUAL, 0, constrName("c09b: ze" + p + "-" + d + "-zb" + p + "-" + d + " <= q.w" + p + "-" + d));
                }
            }

            // aggiungo vincoli (10)(11)
            for (int p = 0; p < inst.getN_physicians(); p++) {
                for (int s = 0; s < inst.getN_weeks(); s++) {
                    expr = new GRBLinExpr();
                    for (int d = 7 * s; d < 7 * (s + 1); d++) {
                        expr.addTerm(1, ze[p][d]);
                        expr.addTerm(-1, zb[p][d]);
                    }
                    model.addConstr(expr, GRB.LESS_EQUAL, inst.getFMAX()[p],
                            constrName("c12: timeslot medico" + p + "nella settimana" + s + "<= Fmax"));

                    expr = new GRBLinExpr();
                    for (int d = 7 * s; d < 7 * (s + 1); d++) {
                        expr.addTerm(1, ze[p][d]);
                        expr.addTerm(-1, zb[p][d]);
                    }
                    model.addConstr(expr, GRB.GREATER_EQUAL, inst.getFMIN()[p],
                            constrName("c13: timeslot medico" + p + "nella settimana" + s + "Fmin"));
                }
            }

            // aggiungo vincoli (12)(13)
            for (int p = 0; p < inst.getN_physicians(); p++) {
                for (int j = 0; j < inst.getN_jobs(); j++)
                    if (PJ[p][j] > 0) {
                        for (int s = 0; s < inst.getN_weeks(); s++) {
                            expr = new GRBLinExpr();
                            for (int d = 7 * s; d < 7 * (s + 1); d++) {
                                expr.addTerm(1, x[p][j][d]);
                            }
                            model.addConstr(expr, GRB.GREATER_EQUAL, inst.getLMIN()[p][j],
                                    constrName("c14: physician" + p + " performs" +
                                            "task" + j + " at least Lmin in week" + s));

                            expr = new GRBLinExpr();
                            for (int d = 7 * s; d < 7 * (s + 1); d++) {
                                expr.addTerm(1, x[p][j][d]);
                            }
                            model.addConstr(expr, GRB.LESS_EQUAL, inst.getLMAX()[p][j],
                                    constrName("c15: physician" + p + " performs" +
                                            "task" + j + " at most Lmax in week" + s));
                        }
                    }
            }

            // aggiungo vincoli (14)(15)
            for (int p = 0; p < inst.getN_physicians(); p++) {
                for (int s = 0; s < inst.getN_weeks(); s++) {
                    expr = new GRBLinExpr();
                    for (int d = 7 * s; d < 7 * (s + 1); d++) {
                        expr.addTerm(1, w[p][d]);
                    }
                    model.addConstr(expr, GRB.GREATER_EQUAL, inst.getUMIN()[p],
                            constrName("c16: physician" + p + " works at least Umin days in week " + s));

                    expr = new GRBLinExpr();
                    for (int d = 7 * s; d < 7 * (s + 1); d++) {
                        expr.addTerm(1, w[p][d]);
                    }
                    model.addConstr(expr, GRB.LESS_EQUAL, inst.getUMAX()[p],
                            constrName("c16: physician" + p + " works at most Umax days in week " + s));
                }
            }

            // aggiungo vincoli (16)(17)
            for (int p = 0; p < inst.getN_physicians(); p++) {
                for (int d = 0; d < inst.getN_days(); d++) {
                    expr = new GRBLinExpr();
                    for (int j = 0; j < inst.getN_jobs(); j++) {
                        expr.addTerm(1, x[p][j][d]);
                    }
                    expr.addTerm(-inst.getN_jobs(), w[p][d]);
                    model.addConstr(expr, GRB.LESS_EQUAL, 0, constrName("c18a: physician "+p+" day "+d));

                    expr = new GRBLinExpr();
                    for (int j = 0; j < inst.getN_jobs(); j++) {
                        expr.addTerm(1, x[p][j][d]);
                    }
                    expr.addTerm(-1, w[p][d]);
                    model.addConstr(expr, GRB.GREATER_EQUAL, 0, constrName("c18b: physician "+p+" day "+d));
                }
            }

            //vincolo (18)
            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int j = 0; j < inst.getN_jobs(); j++)
                    for (int h = 0; h < inst.getSH()[j]; h++) {
                        expr = new GRBLinExpr();
                        for (int d = inst.getISH()[j][h]; d <= inst.getISH()[j][h] + inst.getdSH()[j][h] - 1; d++) {
                            expr.addTerm(1.0, x[p][j][d-1]);
                        }
                        expr.addTerm(-inst.getdSH()[j][h], x[p][j][inst.getISH()[j][h] - 1]);
                        model.addConstr(expr, GRB.EQUAL, 0, constrName("c19: shift " + h + " for task " + j + " is respected by physician " + p));
                    }


            //vincolo (19)
            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int j = 0; j < inst.getN_jobs(); j++)
                    for (int h = 1; h < inst.getSH()[j]; h++) {
                        expr = new GRBLinExpr();
                        expr.addTerm(1.0, x[p][j][inst.getISH()[j][h] - 1]);
                        expr.addTerm(1.0, x[p][j][inst.getISH()[j][h - 1] - 1]);
                        model.addConstr(expr, GRB.LESS_EQUAL, 1, constrName("c20: physician" + p + "can not perform " +
                                "two consecutive shifts for the task " + j + " shift " + h));
                    }


            // metto a zero le variabili che non dovrebbero essere definite
            for (int p = 0; p < inst.getN_physicians(); p++)
                for (int j = 0; j < inst.getN_jobs(); j++)
                    for (int d = 0; d < inst.getN_days(); d++) {
                        if (PJ[p][j] < 1) {
                            expr = new GRBLinExpr();
                            expr.addTerm(1.0, x[p][j][d]);
                            model.addConstr(expr, GRB.EQUAL, 0, constrName("fissata x"+p+"-"+j+"-"+d));
                        }
                        for (int t = 0; t < inst.getN_timeslots(); t++) {
                            if (PJ[p][j] < 1 || !TJ.get(j).get(d).contains(t + 1)) {
                                expr = new GRBLinExpr();
                                expr.addTerm(1.0, y[p][j][d][t]);
                                model.addConstr(expr, GRB.EQUAL, 0, constrName("fissata y"+p+"-"+j+"-"+d+"-"+t));
                            }
                        }
                    }

            model.update();
            model.write(dest_file+"_PLI.mps");
            GRBModel relaxed1 = model.relax();
			relaxed1.write(dest_file+"_PL.mps");
        }
        catch (GRBException e) {
            System.out.println("Error code: " + e.getErrorCode() + ". " + e.getMessage());
        }

        System.out.println("HO COSTRUITO IL MODELL0");

    }


    private static String constrName(String s) {
        return s.replaceAll(" ", "_").replaceAll(":", "=>");
    }
}
