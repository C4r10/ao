package gurobi;

import java.util.Arrays;

public class SpecJob implements Euristica {

	private double prob=0.0909091;
	private double intervallo=0.0909091;

	@Override
	public void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException {
		// TODO Auto-generated method stub

		Struttura[] jobs = new Struttura[inst.getN_jobs()];

		int[][] PJ = inst.getPJ();

		for (int j = 0; j < inst.getN_jobs(); j++) {
			Struttura uno = new Struttura(j);
			jobs[j] = uno;
			int costo = 0;
			// costo = numero di medici che possono svolgere il lavoro
			for (int p = 0; p < inst.getN_physicians(); p++) {

				costo += PJ[p][j];

			}
			uno.setCosto(costo);
			// System.out.println("Costo:" + costo);
		}

		// Ordino i lavori in base alla specializzazione (in quanti sanno farlo)
		Arrays.sort(jobs, new Ordina());

		// Fisso le variabili dei migliori
		for (int p = 0; p < inst.getN_physicians(); p++) {
			// double rand = Math.random();

			for (int d = 0; d < inst.getN_days(); d++) {
				// if (rand > (1 - percent)) {
				model.setW(p, d);

				for (int i = jobs.length - 1; i > jobs.length * (1 - percent); i--) {
					// // if (rand1 > (1 - percent)) {
					// //System.out.println(jobs[i].getCosto());
					int j = jobs[i].getIndice();
					model.setX(p, j, d);
					//
				}
			}
		}

		// Definisci i vincoli riguardanti i lavori peggiori

		int i = 0;

		System.out.println("------------------- " + jobs[i].getCosto());
		int j = jobs[i].getIndice();

		// impedisco al medico p di svolgere quel job j in d se Q_pd_min=0
		for (int d = 0; d < inst.getN_days(); d++) {

			for (int p = 0; p < inst.getN_physicians(); p++) {
				double rand = Math.random();
				if (rand > (percent)) {
					if (inst.getQMIN()[p][d] == 0 && model.getXVal(p, j, d) == 1) {
						model.addConsSpecJob(p, j, d);
					}
				}
			}
		}
	}

	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	public double getIntervallo() {
		// TODO Auto-generated method stub
		return intervallo;
	}

	public void setIntervallo(double intervallo) {
		this.intervallo = intervallo;
	}

}
