/**
 *
 */
package gurobi;

/**
 * @author Carlo
 * 20 ott 2016
 *
 */
public interface Euristica {

	void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException;
	double getProb();
	void setProb(double prob);
	double getIntervallo();
	void setIntervallo(double prob);
}
