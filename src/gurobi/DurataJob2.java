/**
 *
 */

//DA FARE

package gurobi;

import java.util.Arrays;
import java.util.Vector;

/**
 * @author Carlo 20 ott 2016
 *
 */
public class DurataJob2 implements Euristica {

	private double prob=0.0909091;
	private double intervallo=0.0909091;

	@Override
	public void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException {
		// TODO Auto-generated method stub

		Struttura[] medici = new Struttura[inst.getN_physicians()];
		Vector<Vector<Vector<Integer>>> TJ = inst.getTJ();
		int[][] matriceD = inst.getMatrix_d();

		for (int p = 0; p < inst.getN_physicians(); p++) {
			Struttura uno = new Struttura(p);
			medici[p] = uno;
			double costo = 0;
			for (int j = 0; j < inst.getN_jobs(); j++) {
				for (int d = 0; d < inst.getN_days(); d++) {
					costo += model.getXVal(p, j, d) * matriceD[j][d];
					// System.out.println("setto costo:"+model.getXVal(p, j, d)
					// * matriceD[j][d]);
				}
			}
			uno.setCosto(costo);
		}

		Arrays.sort(medici, new Ordina());

		// Fisso le variabili tranne le y
		for (int i = medici.length - 1; i > medici.length * (1 - percent); i--) {

			System.out.println(medici[i].getCosto());
			int p = medici[i].getIndice();

			for (int d = 0; d < inst.getN_days(); d++) {

				model.setW(p, d);
				// model.setZB(p, d);
				// model.setZE(p, d);

				for (int j = 0; j < inst.getN_jobs(); j++) {
					model.setX(p, j, d);
				}
			}
		}

		// Definisci i vincoli riguardanti i lavori
		// for (int i = 0; i < medici.length * percent; i++) {

		System.out.println("------------------- " + medici[0].getCosto());
		int p = medici[inst.getN_physicians() - 1].getIndice();

		for (int d = 0; d < inst.getN_days(); d++) {
			for (int j = 0; j < inst.getN_jobs(); j++) {
				for (int t = 0; t < inst.getN_timeslots(); t++) {
					int n = TJ.get(j).get(d).size();

					if (model.getYVal(p, j, d, t) == 1 && n > 1) {
						model.addConsStartJob(p, j, d, t);
					}
				}
			}
		}
		// }
	}

	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	@Override
	public double getIntervallo() {
		// TODO Auto-generated method stub
		return intervallo;
	}

	@Override
	public void setIntervallo(double intervallo) {
		this.intervallo = intervallo;

	}
}
