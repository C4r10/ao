/**
 *
 */
package gurobi;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Carlo
 * 20 ott 2016
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		try {
			
			String indice="10";
			
			Lettore istanza = new Lettore("istanze/inst"+indice+".txt");

			GRBEnv env = new GRBEnv("GUROBI.log");
			System.out.println("---------------------------------------------------------------------------------------");
			System.out.println("-----------------ACQUISISCO IL MODELLO---------------");
			System.out.println("---------------------------------------------------------------------------------------");
			
			GRBModel model = new GRBModel(env, "Modelli/00"+indice+"/modello_00"+indice+"_PLI.mps");
			
			ModelVar mio= new ModelVar(model, istanza);
			System.out.println("---------------------------------------------------------------------------------------");
			System.out.println("-----------------MODEL VAR---------------");
			System.out.println("---------------------------------------------------------------------------------------");

			Alns alns= new Alns();
			System.out.println("---------------------------------------------------------------------------------------");
			System.out.println("-----------------ALNS START---------------");
			System.out.println("---------------------------------------------------------------------------------------");
			
			alns.run(mio,indice, "IntSol/00"+indice+"/sol_intera1.sol", istanza);

		} catch (GRBException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
