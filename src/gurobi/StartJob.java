package gurobi;

import java.util.Arrays;
import java.util.Vector;

public class StartJob implements Euristica {
	/*
	 * (non-Javadoc)
	 * 
	 * @see gurobi.Euristica#ordina(gurobi.ModelVar, gurobi.Lettore)
	 */

	private double prob=0.0909091;
	private double intervallo=0.0909091;

	@SuppressWarnings("unchecked")
	@Override
	public void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException {
		// TODO Auto-generated method stub

		Struttura[] medici = new Struttura[inst.getN_physicians()];

		int[][] matriceD = inst.getMatrix_d();
		Vector<Vector<Vector<Integer>>> TJ = inst.getTJ();

		GRBVar[][] ze = model.getZe();
		GRBVar[][] zb = model.getZb();

		double tot = 0;
		double durata = 0;
		double jobs = 0;
		double gg = 0;

		for (int p = 0; p < inst.getN_physicians(); p++) {
			Struttura uno = new Struttura(p);
			medici[p] = uno;
			double costo = 0;

			for (int d = 0; d < inst.getN_days(); d++) {
				double Ze = ze[p][d].get(GRB.DoubleAttr.X);
				double Zb = zb[p][d].get(GRB.DoubleAttr.X);

				costo += Ze - Zb;
				gg = Ze - Zb;
				durata += Ze - Zb;
				// Time slot che il medico esame sta in ospedale senza lavorare
				for (int j = 0; j < inst.getN_jobs(); j++) {
					costo -= model.getXVal(p, j, d) * matriceD[j][d];
					jobs += model.getXVal(p, j, d) * matriceD[j][d];
					gg -= model.getXVal(p, j, d) * matriceD[j][d];
				}
			}

			uno.setCosto(costo);
			tot += costo;
		}

		// Ordino i medici in base alle ore di pausa retribuite
		Arrays.sort(medici, new Ordina());

		// Fisso le variabili al loro valore per i medici che hanno poche pause
		for (int i = medici.length - 1; i > medici.length * (1 - percent); i--) {

			int p = medici[i].getIndice();
			System.out.println("Medico: " + p + " #pause: " + medici[i].getCosto());

			for (int d = 0; d < inst.getN_days(); d++) {
				model.setW(p, d);

				for (int j = 0; j < inst.getN_jobs(); j++) {
					model.setX(p, j, d);
					// for (int t = 0; t < inst.getN_timeslots(); t++) {
					// setY(p, j, d, t);
					// }
				}

			}
		}

		// Quelli che hanno pause alte non li faccio iniziare pi� in quelle ore

		int p = medici[0].getIndice();

		for (int d = 0; d < inst.getN_days(); d++) {

			for (int j = 0; j < inst.getN_jobs(); j++) {
				for (int t = 0; t < inst.getN_timeslots(); t++) {
					int n = TJ.get(j).get(d).size();

					// se un lavoro inizia nel timeslot t e TJ ha size>1
					// (ho altre possibilit� di timeslot in cui inizare)
					// metto vincolo che non pu� iniziare in t

					// lo faccio solo per una percentuale
					if (model.getYVal(p, j, d, t) == 1 && n > 1) {
						model.addConsStartJob(p, j, d, t);
					}
				}

			}
		}
	}

	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	public double getIntervallo() {
		// TODO Auto-generated method stub
		return intervallo;
	}

	public void setIntervallo(double intervallo) {
		this.intervallo = intervallo;
	}
}
