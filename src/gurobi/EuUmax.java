package gurobi;

import java.util.Arrays;

public class EuUmax implements Euristica {
	/*
	 * (non-Javadoc)
	 * 
	 * @see gurobi.Euristica#ordina(gurobi.ModelVar, gurobi.Lettore)
	 */

	private double prob=0.0909091;
	private double intervallo=0.0909091;
	
	@Override
	public void ordFix(ModelVar model, Lettore inst, double percent) throws GRBException {
		// TODO Auto-generated method stub

		Struttura[][] medici = new Struttura[inst.getN_weeks()][inst.getN_physicians()];
		int[] Umax = inst.getUMAX();
		int[][] matriceD = inst.getMatrix_d();
		GRBVar[][] W = model.getW();

		for (int w = 0; w < inst.getN_weeks(); w++) {

			for (int p = 0; p < inst.getN_physicians(); p++) {
				
				int costo = 0;
				Struttura uno = new Struttura(p);
				medici[w][p] = uno;

				for (int d = w * 7; d < w * 7 + 7; d++) {
					if (model.getWVal(p, d) == 1) {
						costo += 1;
					}
				}
				uno.setCosto(Umax[p] - costo);
			}
		}

		for (int w = 0; w < inst.getN_weeks(); w++) {
			Arrays.sort(medici[w], new Ordina());
		}

		// Fisso le variabili
		for (int w = 0; w < inst.getN_weeks(); w++) {
			for (int i = 0; i < medici.length * (percent); i++) {

				int p = medici[w][i].getIndice();
				for (int d = w * 7; d < w * 7 + 7; d++) {
					model.setW(p, d);
					// setZB(p, d);
					// setZE(p, d);
					for (int j = 0; j < inst.getN_jobs(); j++) {
						model.setX(p, j, d);
						// for (int t = 0; t < inst.getN_timeslots(); t++) {
						// setY(p, j, d, t);
						// }
					}
				}
			}
		}

		for (int w = 0; w < inst.getN_weeks(); w++) {
			GRBLinExpr expr = new GRBLinExpr();
			int n = 0;
			int p = medici[w][inst.getN_physicians() - 1].getIndice();
			System.out.println("-----------------------");
			System.out.println("Vincolo: " + medici[w][inst.getN_physicians() - 1].getCosto());
			System.out.println("-----------------------");

			// Definisci i vincoli riguardanti i lavori
			for (int d = w * 7; d < w * 7 + 7; d++) {

				// RIMUOVO UN JOB SOLO AL MEDICO PEGGIORE

				if (model.getWVal(p, d) == 1) {
					n = n + 1;
					expr.addTerm(1.0, W[p][d]);

				}
			}
			model.addCons(expr, n - 1);
		}
	}

	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	public double getIntervallo() {
		// TODO Auto-generated method stub
		return intervallo;
	}

	public void setIntervallo(double intervallo) {
		intervallo = intervallo;
	}
}